import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Announcement from '../views/Announcement.vue'
import Cooperate from '../views/Cooperate.vue'
import TeamWork from '../views/TeamWork.vue'
import About from '../views/About.vue'
import Cart from '../views/Cart.vue'
import Mail from '../views/Mail.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component:About,
  },
  {
    path:'/TeamWork',
    name:'TeamWork',
    component:TeamWork,
  },
  {
    path:'/Cooperate',
    name:'Cooperate',
    component:Cooperate,
  },
  {
    path:'/Announcement',
    name:'Announcement',
    component:Announcement,
  },
  {
    path:'/Cart',
    name:'Cart',
    component:Cart,
  },
  {
    path:'/Mail',
    name:'Mail',
    component:Mail,
  },
]

const router = new VueRouter({
  routes
})

export default router
