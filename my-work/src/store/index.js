import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    screenWidth:window.innerWidth,//監聽螢幕大小
    isLogin:false,//登入狀態判斷
    isLoading:true,//讀取中
  },
  mutations: {
    setScreenWidth:function(state,payload){
      state.screenWidth=payload;
    },
    setIsLoading:function(state,payload){
      if(payload==true){
        state.isLoading=true;
      }else{
        state.isLoading=false;
      }
    }
  },
  actions: {
    getScreenWidth:({commit},payload)=>{
        commit("setScreenWidth",payload);
    },
    changeIsLoading:({commit})=>{
      commit("setIsLoading",true);
      setTimeout(function(){
        commit("setIsLoading",false);
      },1000)
    }
  },
  modules: {
  }
})
